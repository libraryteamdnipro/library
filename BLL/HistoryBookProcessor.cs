﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Models;
using DAL.Models;
using DAL.SQL;

namespace BLL
{
    public class HistoryBookProcessor
    {
        public HistoryBookProcessor() { }
        
        public HistoryBusinessModel GetHistoryOfBook(string title)
        {
            HistorySQL historySQL = new HistorySQL();
            return new HistoryBusinessModel(historySQL.GetHistoryOfBook(title));
        }

        public HistoryBusinessModel GetHistoryOfBook(int id)
        {
            HistorySQL historySQL = new HistorySQL();
            return new HistoryBusinessModel(historySQL.GetHistoryOfBook(id));
        }

        public void SetHistoryOfBook(HistoryBusinessModel history)
        {
            HistorySQL historySQL = new HistorySQL();
            HistoryModel historyDataModel = new HistoryModel(history.Book);
            foreach (BHBusinessModel item in history.BookHistories)
            {
                BookHistoryModel bookHistoryModel = new BookHistoryModel(item.WhenTaken, item.WhenReturned);
                bookHistoryModel.UserId = item.UserId;
                historyDataModel.BookHistories.Add(bookHistoryModel);
            }
            historySQL.SetBookHistiry(historyDataModel);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;
using DAL.SQL;
using BLL.Models;

namespace BLL
{
    public class BooksProcessor
    {
        public BooksProcessor() { }

        public List<BookBusinessModel> GetAllBook()
        {
            BookSQL bookSQL = new BookSQL();
            List<BookBusinessModel> books = new List<BookBusinessModel>();
            List<BookModel> getedBooks = bookSQL.GetAllBooks();
            foreach (BookModel book in getedBooks)
            {
                books.Add(new BookBusinessModel(book));
            }
            return books;
        }

        public List<BookBusinessModel> SortBooksByName(List<BookBusinessModel> books)
        {
            List<BookBusinessModel> sortedBooks = books.OrderBy(x => x.Title).ToList();
            return sortedBooks;
        }

        public List<BookBusinessModel> SortBooksByAuthor(List<BookBusinessModel> books)
        {
            List<BookBusinessModel> sortedBooks = new List<BookBusinessModel>();
            foreach (BookBusinessModel book in sortedBooks)
            {
                book.Authors = book.Authors.OrderBy(x => x).ToList();
            }
            sortedBooks = books.OrderBy(x => x.Authors[0]).ToList();
            return sortedBooks;
        }

        public BookBusinessModel GetBookByTitle(string title)
        {
            BookSQL bookSQL = new BookSQL();
            BookBusinessModel book = new BookBusinessModel(bookSQL.GetBookByTitle(title));
            return book;
        }

        public List<BookBusinessModel> GetBookByAuthor(string author)
        {
            BookSQL bookSQL = new BookSQL();
            List<BookModel> bookModels = bookSQL.GetBooksByAuthor(author);
            List<BookBusinessModel> books = new List<BookBusinessModel>();
            foreach (BookModel book in bookModels)
            {
                books.Add(new BookBusinessModel(book));
            }
            return books;
        }

        public void SetBook(BookBusinessModel book)
        {
            BookSQL bookSQL = new BookSQL();
            BookModel bookModel = new BookModel(book.Title, book.IdUser);
            bookModel.Authors = book.Authors;
            bookSQL.CreateBook(bookModel);
        }

        public void TakeBook(string title, int idUser)
        {
            BookSQL bookSQL = new BookSQL();
            bookSQL.SetUserOfBook(title, idUser);
        }

        public void ReturnedBook(string title)
        {
            BookSQL bookSQL = new BookSQL();
            bookSQL.SetUserOfBook(title, 0);
        }

        public void DeleteBook(string title)
        {
            BookModel book = new BookModel(title);
            BookSQL bookSQL = new BookSQL();
            bookSQL.DeleteBook(book);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class EmailSender
    {
        private string Smtp { get; }
        private string From { get; }
        private string Subject { get; }
        private string Body { get; }
        private int Port { get; set; }

        public EmailSender()
        {
            Smtp = "smtp.gmail.com";
            From = "LibaryTestMail@gmail.com";
            Subject = "Test Mail";
            Body = "Send by programm";
            Port = 587;
        }

        public void SendMail(string email)
        {
            using (MailMessage mail = new MailMessage())
            {
                SmtpClient SmtpServer = new SmtpClient(Smtp);

                mail.From = new MailAddress(From);
                mail.To.Add(email);
                mail.Subject = Subject;
                mail.Body = Body;

                SmtpServer.Port = Port;
                SmtpServer.Credentials = new System.Net.NetworkCredential("LibaryTestMail@gmail.com", "Qpwo1029");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
            }
        }
         
    }
}

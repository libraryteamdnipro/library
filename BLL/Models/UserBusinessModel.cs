﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace BLL.Models
{
    public class UserBusinessModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public List<string> Books { get; set; }

        public UserBusinessModel()
        {
            Books = new List<string>();
        }
        public UserBusinessModel(string name, string email) : this()
        {
            Name = name;
            Email = email;
        }

        public UserBusinessModel(string name, string email, List<string> books) : this(name, email)
        {
            Books = books;
        }

        public UserBusinessModel(UserModel userModel)
        {
            Id = userModel.Id;
            Name = userModel.Name;
            Email = userModel.Email;
            Books = userModel.Books;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace BLL.Models
{
    public class BookBusinessModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int IdUser { get; set; }
        public List<string> Authors { get; set; }

        public BookBusinessModel()
        {
            Authors = new List<string>();
        }
        public BookBusinessModel(string title) : this()
        {
            Title = title;
        }
        public BookBusinessModel(string title, int idUser) : this(title)
        {
            IdUser = idUser;
        }
        public BookBusinessModel(BookModel bookModel)
        {
            Id = bookModel.Id;
            Title = bookModel.Title;
            IdUser = bookModel.IdUser;
            Authors = bookModel.Authors;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace BLL.Models
{
    public class AuthorBusinessModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public List<string> Books { get; set; }

        public AuthorBusinessModel()
        {
            Books = new List<string>();
        }
        public AuthorBusinessModel(string name) : this()
        {
            List<string> temp = name.Split(' ').ToList();
            Name = temp[0];
            Surname = temp[1];
        }
        public AuthorBusinessModel(string name, string surname) : this()
        {
            Name = name;
            Surname = surname;
        }
        public AuthorBusinessModel(AuthorModel authorModel)
        {
            Id = authorModel.Id;
            Name = authorModel.Name;
            Surname = authorModel.Surname;
            Books = authorModel.Books;
        }
    }
}

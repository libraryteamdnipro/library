﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace BLL.Models
{
    public class BHBusinessModel
    {
        public int UserId { get; set; }
        public DateTime WhenTaken { get; set; }
        public DateTime WhenReturned { get; set; }

        public BHBusinessModel() { }
        public BHBusinessModel(DateTime taken)
        {
            WhenTaken = taken;
        }
        public BHBusinessModel(DateTime taken, DateTime returned) : this(taken)
        {
            WhenReturned = returned;
        }
        public BHBusinessModel(BookHistoryModel bookHistoryModel)
        {
            UserId = bookHistoryModel.UserId;
            WhenTaken = bookHistoryModel.WhenTaken;
            WhenReturned = bookHistoryModel.WhenReturned;
        }
    }
}

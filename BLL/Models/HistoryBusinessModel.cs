﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace BLL.Models
{
    public class HistoryBusinessModel
    {
        public int Id { get; set; }
        public string Book { get; set; }
        public List<BHBusinessModel> BookHistories { get; set; }

        public HistoryBusinessModel()
        {
            BookHistories = new List<BHBusinessModel>();
        }
        public HistoryBusinessModel(string book) : this()
        {
            Book = book;
        }
        public HistoryBusinessModel(string book, List<BHBusinessModel> bookHistories) : this(book)
        {
            BookHistories = bookHistories;
        }
        public HistoryBusinessModel(HistoryModel history)
        {
            Id = history.Id;
            Book = history.Book;
            foreach (BookHistoryModel item in history.BookHistories)
            {
                BookHistories.Add(new BHBusinessModel(item));
            }
        }
    }
}

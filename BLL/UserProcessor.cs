﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Models;
using DAL.Models;
using DAL.SQL;

namespace BLL
{
    public class UserProcessor
    {
        public UserProcessor() { }

        public List<UserBusinessModel> GetAllUser()
        {
            List<UserBusinessModel> users = new List<UserBusinessModel>();
            UserSQL userSQL = new UserSQL();
            List<UserModel> usersModel = userSQL.GetAllUsers();
            foreach (UserModel item in usersModel)
            {
                users.Add(new UserBusinessModel(item));
            }
            return users;
        }

        public UserBusinessModel GetUserById(int id)
        {
            UserSQL userSQL = new UserSQL();
            return new UserBusinessModel(userSQL.GetUserById(id));
        }

        public UserBusinessModel GetUserByEmail(string email)
        {
            UserSQL userSQL = new UserSQL();
            return new UserBusinessModel(userSQL.GetUserByEmail(email));
        }

        public void CreateUser(UserBusinessModel user)
        {
            UserModel userModel = new UserModel(user.Name, user.Email, user.Books);
            UserSQL userSQL = new UserSQL();
            userSQL.CreateUser(userModel);
        }

        public void DeleteUser(string email)
        {
            UserSQL userSQL = new UserSQL();
            userSQL.DeleteUser(email);
        }
    }
}

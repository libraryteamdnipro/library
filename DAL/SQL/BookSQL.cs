﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL.SQL
{
    public class BookSQL
    {
        private string _ConnectionString { get; }

        public BookSQL()
        {
            _ConnectionString = "Data Source=MSI;Initial Catalog=Library;Integrated Security=True";
        }

        public List<BookModel> GetAllBooks()
        {
            List<BookModel> books = new List<BookModel>();

            using (SqlConnection conn = new SqlConnection())
            {
                List<AuthorModel> authors = new List<AuthorModel>();
                conn.ConnectionString = _ConnectionString;
                SqlCommand command = new SqlCommand("SELECT * FROM Books", conn);
                conn.Open();
                using (SqlDataReader bookReader = command.ExecuteReader())
                {
                    while (bookReader.Read())
                    {
                        BookModel book = new BookModel(bookReader[1].ToString());
                        book.Id = (int)bookReader[0];
                        int idUserTemp;
                        if (int.TryParse(bookReader[2].ToString(),out idUserTemp))
                        {
                            book.IdUser = idUserTemp;
                        }
                        books.Add(book);
                    }
                }
                List<AuthorOfBook> authorOfBook = new List<AuthorOfBook>();
                SqlCommand sqlCommand = new SqlCommand("SELECT * FROM AuthorsOfBooks", conn);
                using (SqlDataReader authorOfBookReader = sqlCommand.ExecuteReader())
                {
                    while (authorOfBookReader.Read())
                    {
                        authorOfBook.Add(new AuthorOfBook((int)authorOfBookReader[0], (int)authorOfBookReader[1]));
                    }
                }
                SqlCommand authorCommand = new SqlCommand("SELECT * FROM Authors", conn);
                using (SqlDataReader authorReader = authorCommand.ExecuteReader())
                {
                    while (authorReader.Read())
                    {
                        List<AuthorOfBook> selektedBooks = authorOfBook.Where(x => x.IdAuthor == (int)authorReader[0]).ToList();

                        foreach (BookModel book in books)
                        {
                            foreach (AuthorOfBook author in selektedBooks)
                            {
                                if (book.Id == author.IdBook)
                                {
                                    book.Authors.Add(authorReader[1].ToString());
                                }
                            }
                        }
                    }
                }
            }

            return books;
        }

        public List<BookModel> GetBooksByAuthor(string author)
        {
            List<BookModel> books = new List<BookModel>();
            using (SqlConnection conn = new SqlConnection())
            {
                List<int> authorsBooks = new List<int>();
                int authorId = 0;
                conn.ConnectionString = _ConnectionString;
                conn.Open();
                SqlCommand authorCommand = new SqlCommand("SELECT * FROM Authors WHERE Name = @name", conn);
                authorCommand.Parameters.Add(new SqlParameter("name", author));
                using (SqlDataReader authorReader = authorCommand.ExecuteReader())
                {
                    while (authorReader.Read())
                    {
                        authorId = (int)authorReader[0];
                    }
                }
                SqlCommand sqlCommand = new SqlCommand("SELECT * FROM AuthorsOfBooks WHERE IdAuthors = @idAuthor", conn);
                sqlCommand.Parameters.Add(new SqlParameter("idAuthor", authorId));
                using (SqlDataReader authorOfBookReader = sqlCommand.ExecuteReader())
                {
                    while (authorOfBookReader.Read())
                    {
                        authorsBooks.Add((int)authorOfBookReader[1]);
                    }
                }
                SqlCommand command = new SqlCommand("SELECT * FROM Books", conn);
                using (SqlDataReader bookReader = command.ExecuteReader())
                {
                    while (bookReader.Read())
                    {
                        if (authorsBooks.FirstOrDefault(x => x == (int)bookReader[0]) == (int)bookReader[0])
                        {
                            BookModel book = new BookModel(bookReader[1].ToString());
                            book.Id = (int)bookReader[0];
                            int idUserTemp;
                            if (int.TryParse(bookReader[2].ToString(), out idUserTemp))
                            {
                                book.IdUser = idUserTemp;
                            }
                            books.Add(book);
                            book.Authors.Add(author);
                        }
                    }
                }
            }
            return books;
        }

        public BookModel GetBookByTitle(string title)
        {
            BookModel book = new BookModel();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = _ConnectionString;
                SqlCommand command = new SqlCommand("SELECT * FROM Books WHERE Title = @title", conn);
                command.Parameters.Add(new SqlParameter("title", title));
                conn.Open();
                using (SqlDataReader bookReader = command.ExecuteReader())
                {
                    while (bookReader.Read())
                    {
                        book.Id = (int)bookReader[0];
                        book.Title = bookReader[1].ToString();
                        int idUserTemp;
                        if (int.TryParse(bookReader[2].ToString(), out idUserTemp))
                        {
                            book.IdUser = idUserTemp;
                        }
                    }
                }
                List<int> authorOfBook = new List<int>();
                SqlCommand sqlCommand = new SqlCommand("SELECT * FROM AuthorsOfBooks WHERE IdBook = @idBook", conn);
                sqlCommand.Parameters.Add(new SqlParameter("idBook", book.Id));
                using (SqlDataReader authorOfBookReader = sqlCommand.ExecuteReader())
                {
                    while (authorOfBookReader.Read())
                    {
                        authorOfBook.Add((int)authorOfBookReader[0]);
                    }
                }
                SqlCommand authorCommand = new SqlCommand("SELECT * FROM Authors", conn);
                using (SqlDataReader authorReader = authorCommand.ExecuteReader())
                {
                    while (authorReader.Read())
                    {
                        foreach (int item in authorOfBook)
                        {
                            if ((int)authorReader[0] == item)
                            {
                                book.Authors.Add(authorReader[1].ToString());
                            }
                        }
                    }
                }
            }
            return book;
        }

        public void CreateBook(BookModel book)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                List<int> authorId = new List<int>();
                List<String> newAuthors = new List<string>();
                conn.ConnectionString = _ConnectionString;
                conn.Open();
                SqlCommand command = new SqlCommand("INSERT INTO Books (Title, IdUser) VALUES (@title, @idUser)", conn);
                command.Parameters.Add(new SqlParameter("title", book.Title));
                command.Parameters.Add(new SqlParameter("idUser", book.IdUser));
                command.ExecuteNonQuery();
                conn.Close();
                conn.Open();
                SqlCommand selectCommand = new SqlCommand("SELECT * FROM Books WHERE Title = @title", conn);
                selectCommand.Parameters.Add(new SqlParameter("title", book.Title));
                using (SqlDataReader bookReader = selectCommand.ExecuteReader())
                {
                    while (bookReader.Read())
                    {
                        if (book.Title == bookReader[1].ToString())
                        {
                            book.Id = (int)bookReader[0];
                        }
                    }
                }
                SqlCommand authorCommand = new SqlCommand("SELECT * FROM Authors", conn);
                using (SqlDataReader authorReader = authorCommand.ExecuteReader())
                {
                    bool isAuthorInTable;
                    foreach (string item in book.Authors)
                    {
                        isAuthorInTable = false;
                        while (authorReader.Read())
                        {
                            if (authorReader[1].ToString() == item)
                            {
                                authorId.Add((int)authorReader[0]);
                                isAuthorInTable = true;
                            }
                        }
                        if (!isAuthorInTable)
                        {
                            newAuthors.Add(item);
                        }
                    }
                }
                if (newAuthors != null)
                {
                    foreach (string item in newAuthors)
                    {
                        SqlCommand insAuthorCommand = new SqlCommand("INSERT INTO Authors (Name) VALUES (@name)", conn);
                        insAuthorCommand.Parameters.Add(new SqlParameter("name", item));
                        insAuthorCommand.ExecuteNonQuery();
                        conn.Close();
                        conn.Open();
                        SqlCommand selectAuthorCommand = new SqlCommand("SELECT * FROM Authors WHERE Name = @name", conn);
                        selectAuthorCommand.Parameters.Add(new SqlParameter("name", item));
                        using (SqlDataReader bookReader = selectAuthorCommand.ExecuteReader())
                        {
                            while (bookReader.Read())
                            {
                                authorId.Add((int)bookReader[0]);
                            }
                        }
                    }
                }
                if (authorId != null)
                {
                    foreach (int item in authorId)
                    {
                        SqlCommand insBookAuthorCommand = new SqlCommand("INSERT INTO AuthorsOfBooks (IdAuthors, IdBook) VALUES (@author, @book)", conn);
                        insBookAuthorCommand.Parameters.Add(new SqlParameter("author", item));
                        insBookAuthorCommand.Parameters.Add(new SqlParameter("book", book.Id));
                        insBookAuthorCommand.ExecuteNonQuery();
                    }
                }
            }
        }

        public void SetUserOfBook(string title, int idUser)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                bool isUserFind = false;
                conn.ConnectionString = _ConnectionString;
                conn.Open();
                SqlCommand selectCommand = new SqlCommand("SELECT * FROM Users WHERE Id = @idUser", conn);
                selectCommand.Parameters.Add(new SqlParameter("idUser", idUser));
                using (SqlDataReader userReader = selectCommand.ExecuteReader())
                {
                    while (userReader.Read())
                    {
                        if (idUser == (int)userReader[0])
                        {
                            isUserFind = true;
                        }
                    }
                }
                if (isUserFind)
                {
                    SqlCommand command = new SqlCommand("UPDATE Books SET IdUser = @idUser WHERE Title = @title", conn);
                    command.Parameters.Add(new SqlParameter("title", title));
                    command.Parameters.Add(new SqlParameter("idUser",idUser));
                    command.ExecuteNonQuery();
                }
            }
        }

        public void DeleteBook(BookModel book)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = _ConnectionString;
                conn.Open();
                SqlCommand command2 = new SqlCommand("DELETE FROM AuthorsOfBooks WHERE IdBook = @idBook", conn);
                command2.Parameters.Add(new SqlParameter("idBook", book.Id));
                command2.ExecuteNonQuery();
                SqlCommand command = new SqlCommand("DELETE Books WHERE Title = @title", conn);
                command.Parameters.Add(new SqlParameter("title", book.Title));
                command.ExecuteNonQuery();
            }
        }
        
    }

    class AuthorOfBook
    {
        public int IdAuthor { get; set; }
        public int IdBook { get; set; }

        public AuthorOfBook(int idAuthor, int idBook)
        {
            IdAuthor = idAuthor;
            IdBook = idBook;
        }
    }
    
}
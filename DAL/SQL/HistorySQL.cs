﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL.SQL
{
    public class HistorySQL
    {
        private string _ConnectionString { get; }

        public HistorySQL()
        {
            _ConnectionString = "Data Source=MSI;Initial Catalog=Library;Integrated Security=True";
        }

        public HistoryModel GetHistoryOfBook(string title)
        {
            HistoryModel historyOfBook = new HistoryModel();
            int bookId = 0;
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = _ConnectionString;
                SqlCommand command = new SqlCommand("SELECT * FROM Books WHERE Title = @title", conn);
                command.Parameters.Add(new SqlParameter("title", title));
                conn.Open();
                using (SqlDataReader bookReader = command.ExecuteReader())
                {
                    while (bookReader.Read())
                    {
                        if (bookReader[1].ToString() == title)
                        {
                            bookId = (int)bookReader[0];
                            historyOfBook.Book = title;
                        }
                    }
                }
                SqlCommand command2 = new SqlCommand("SELECT * FROM BookHistory WHERE IdBook = @idBook", conn);
                command2.Parameters.Add(new SqlParameter("idBook", bookId));
                using (SqlDataReader reader = command2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (bookId == (int)reader[1])
                        {
                            BookHistoryModel bookHistory = new BookHistoryModel((DateTime)reader[3], (DateTime)reader[4]);
                            bookHistory.UserId = (int)reader[2];
                            historyOfBook.BookHistories.Add(bookHistory);
                        }
                    }
                }
            }
            return historyOfBook;
        }

        public HistoryModel GetHistoryOfBook(int id)
        {
            HistoryModel historyOfBook = new HistoryModel();
            int bookId = 0;
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = _ConnectionString;
                conn.Open();
                SqlCommand command2 = new SqlCommand("SELECT * FROM BookHistory WHERE IdBook = @idBook", conn);
                command2.Parameters.Add(new SqlParameter("idBook", bookId));
                using (SqlDataReader reader = command2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (bookId == (int)reader[1])
                        {
                            BookHistoryModel bookHistory = new BookHistoryModel((DateTime)reader[3], (DateTime)reader[4]);
                            bookHistory.UserId = (int)reader[2];
                            historyOfBook.BookHistories.Add(bookHistory);
                        }
                    }
                }
            }
            return historyOfBook;
        }

        public void SetBookHistiry(HistoryModel history)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                int idUser = 0;
                int idBook = 0;
                conn.ConnectionString = _ConnectionString;
                conn.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Books WHERE Title = @title", conn);
                command.Parameters.Add(new SqlParameter("title", history.Book));
                conn.Open();
                using (SqlDataReader bookReader = command.ExecuteReader())
                {
                    while (bookReader.Read())
                    {
                        if (bookReader[1].ToString() == history.Book)
                        {
                            idBook = (int)bookReader[0];
                        }
                    }
                }
                SqlCommand insertCommand = new SqlCommand("INSERT INTO BookHistory (IdBook, IdUser, WhenTaken, WhenReturned) VALUES (@idBook, @idUser, @teken, @returned)", conn);
                insertCommand.Parameters.Add(new SqlParameter("idBook", idBook));
                insertCommand.Parameters.Add(new SqlParameter("idUser", history.BookHistories[0].UserId));
                insertCommand.Parameters.Add(new SqlParameter("teken", history.BookHistories[0].WhenTaken));
                insertCommand.Parameters.Add(new SqlParameter("returned", history.BookHistories[0].WhenReturned));
                insertCommand.ExecuteNonQuery();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL.SQL
{
    public class UserSQL
    {
        private string _ConnectionString { get; }

        public UserSQL()
        {
            _ConnectionString = "Data Source=MSI;Initial Catalog=Library;Integrated Security=True";
        }

        public List<UserModel> GetAllUsers()
        {
            List<UserModel> users = new List<UserModel>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = _ConnectionString;
                SqlCommand command = new SqlCommand("SELECT * FROM Users", conn);
                conn.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        UserModel user = new UserModel(reader[1].ToString(), reader[2].ToString());
                        user.Id = (int)reader[0];
                        users.Add(user);
                    }
                }
                SqlCommand bookCommand = new SqlCommand("SELECT * FROM Books", conn);
                using (SqlDataReader bookReader = bookCommand.ExecuteReader())
                {
                    while (bookReader.Read())
                    {
                        foreach (UserModel user in users)
                        {
                            if ((int)bookReader[2] == user.Id)
                            {
                                user.Books.Add(bookReader[1].ToString());
                            }
                        }
                    }
                }
            }
            return users;
        }

        public UserModel GetUserByEmail(string email)
        {
            UserModel user = new UserModel();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = _ConnectionString;
                SqlCommand command = new SqlCommand("SELECT * FROM Users WHERE Email = @email", conn);
                command.Parameters.Add(new SqlParameter("email", email));
                conn.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        user.Id = (int)reader[0];
                        user.Name = reader[1].ToString();
                        user.Email = reader[2].ToString();
                    }
                        
                }
                SqlCommand bookCommand = new SqlCommand("SELECT * FROM Books WHERE IdUser = idUser", conn);
                bookCommand.Parameters.Add(new SqlParameter("idUser",user.Id));
                using (SqlDataReader bookReader = bookCommand.ExecuteReader())
                {
                    while (bookReader.Read())
                    {
                        if ((int)bookReader[2] == user.Id)
                        {
                            user.Books.Add(bookReader[1].ToString());
                        }
                    }
                }
            }
            return user;
        }

        public UserModel GetUserById(int id)
        {
            UserModel user = new UserModel();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = _ConnectionString;
                SqlCommand command = new SqlCommand("SELECT * FROM Users WHERE Id = @id", conn);
                command.Parameters.Add(new SqlParameter("id", id));
                conn.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        user.Id = (int)reader[0];
                        user.Name = reader[1].ToString();
                        user.Email = reader[2].ToString();
                    }

                }
                SqlCommand bookCommand = new SqlCommand("SELECT * FROM Books WHERE IdUser = idUser", conn);
                bookCommand.Parameters.Add(new SqlParameter("idUser", user.Id));
                using (SqlDataReader bookReader = bookCommand.ExecuteReader())
                {
                    while (bookReader.Read())
                    {
                        if ((int)bookReader[2] == user.Id)
                        {
                            user.Books.Add(bookReader[1].ToString());
                        }
                    }
                }
            }
            return user;
        }

        public void CreateUser(UserModel user)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = _ConnectionString;
                conn.Open();
                SqlCommand command = new SqlCommand("INSERT INTO User (Name, Email) VALUES (@name, @email)", conn);
                command.Parameters.Add(new SqlParameter("name", user.Name));
                command.Parameters.Add(new SqlParameter("email", user.Email));
                command.ExecuteNonQuery();
            }
        }

        public void DeleteUser(string email)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = _ConnectionString;
                conn.Open();
                SqlCommand command = new SqlCommand("DELETE FROM User WHERE Email = @email", conn);
                command.Parameters.Add(new SqlParameter("email", email));
                command.ExecuteNonQuery();
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class AuthorModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public List<string> Books { get; set; }

        public AuthorModel()
        {
            Books = new List<string>();
        }
        public AuthorModel(string name) : this()
        {
            List<string> temp = name.Split(' ').ToList();
            Name = temp[0];
            Surname = temp[1];
        }
        public AuthorModel(string name, string surname) : this ()
        {
            Name = name;
            Surname = surname;
        }
    }
}
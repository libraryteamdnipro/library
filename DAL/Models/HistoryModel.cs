﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class HistoryModel
    {
        public int Id { get; set; }
        public string Book { get; set; }
        public List<BookHistoryModel> BookHistories { get; set; }

        public HistoryModel()
        {
            BookHistories = new List<BookHistoryModel>();
        }
        public HistoryModel(string book) : this ()
        {
            Book = book;
        }
        public HistoryModel(string book, List<BookHistoryModel> bookHistories) : this (book)
        {
            BookHistories = bookHistories;
        }
    }
}

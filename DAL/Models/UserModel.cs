﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public List<string> Books { get; set; }

        public UserModel()
        {
            Books = new List<string>();
        }
        public UserModel(string name, string email) : this()
        {
            Name = name;
            Email = email;
        }
        public UserModel(string name, string email, List<string> books) : this(name, email)
        {
            Books = books;
        }
    }
}

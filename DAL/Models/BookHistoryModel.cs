﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class BookHistoryModel
    {
        public int UserId { get; set; }
        public DateTime WhenTaken { get; set; }
        public DateTime WhenReturned { get; set; }

        public BookHistoryModel() { }
        public BookHistoryModel(DateTime taken)
        {
            WhenTaken = taken;
        }
        public BookHistoryModel(DateTime taken,DateTime returned) : this (taken)
        {
            WhenReturned = returned;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class BookModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int IdUser { get; set; }
        public List<string> Authors { get; set; }

        public BookModel()
        {
            Authors = new List<string>();
        }
        public BookModel(string title) : this ()
        {
            Title = title;
        }
        public BookModel(string title, int idUser) : this(title)
        {
            IdUser = idUser;
        }
        public BookModel(string title, int idUser, List<string> authors) : this(title,idUser)
        {
            IdUser = idUser;
            Authors = authors;
        }
    }
}
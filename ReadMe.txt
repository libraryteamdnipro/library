Library application.  Created data access layer in which you can access the database. 
In the business logic layer implemented methods of obtaining books, sorting them, getting books by a particular author(not implemented in UI), 
adding and deleting books and users(not implemented in UI), and adding a history of books(not implemented in UI). 
Added ability to send emails(not implemented in UI).

Backup the database store is located in DBbackup.rar
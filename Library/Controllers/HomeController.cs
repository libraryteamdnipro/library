﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.Models;
using BLL;

namespace Library.Controllers
{
    public class HomeController : Controller
    {
        BooksProcessor booksProcessor;
        public HomeController()
        {
            booksProcessor = new BooksProcessor();
        }

        public ActionResult Index()
        {
            List<BookBusinessModel> bookBusinessModel = booksProcessor.GetAllBook();
            ViewBag.BookBusinessModel = bookBusinessModel;
            return View();
        }

        
        [HttpPost]
        public ActionResult SortByTitle(List<BookBusinessModel> books)
        {
            ViewBag.BookBusinessModel = booksProcessor.SortBooksByName(books);
            return View();
        }
    }
}